/**
 * Hook into events on the module configuration page
 */
(function ($) {
    var baseUrl = 'https://app.velaro.com/';
    $(document).ready(function () {
        var matchVelaroGroup = function () {
            var pageID = $('#edit-velaro-chat-pages option:selected').val();
            var val = $('[name=velaro_chat_page_assignments]').val();
            var currentAssingments = val ? JSON.parse(val) : {};
            var pageAssignment = currentAssingments['page_' + pageID];
            if (pageAssignment) {
                $("#edit-velaro-chat-groups option:selected").attr("selected", false);
                $('#edit-velaro-chat-groups option[value="' + pageAssignment + '"]').attr("selected", true);
            } else {
                $("#edit-velaro-chat-groups option:selected").attr("selected", false);
                $('#edit-velaro-chat-groups option[value="0"]').attr("selected", true);
            }
        };

        $('#edit-velaro-chat-groups').unbind();
        $('#edit-velaro-chat-groups').bind('change', function (e) {
            var pageID = $('#edit-velaro-chat-pages option:selected').val();
            var val = $('[name=velaro_chat_page_assignments]').val();
            var currentAssingments = val ? JSON.parse(val) : {};
            currentAssingments['page_' + pageID] = $('#edit-velaro-chat-groups option:selected').val();
            $('[name=velaro_chat_page_assignments]').attr('value', JSON.stringify(currentAssingments));
        });

        $('#edit-velaro-chat-pages').unbind();
        $('#edit-velaro-chat-pages').bind('change', function (e) {
            matchVelaroGroup();
        });
        $('#velaro-link-account').unbind('click');
        $('#velaro-link-account').bind('click', function () {
            var model = {
                'Username': $('#edit-velaro-chat-username').val(),
                'Password': $('#edit-velaro-chat-password').val()
            };

            $.ajax({
                url: baseUrl + 'api/plugins/login',
                type: 'POST',
                data: JSON.stringify(model),
                contentType: 'application/json',
                dataType: 'json',
                success: function (result) {
                    var loginResult = JSON.parse(result.Content);
                    var siteID = loginResult.Identifier,
                            apiKey = loginResult.ApiKey;
                    $('[name=velaro_chat_site_identifier').val(siteID);
                    $('[name=velaro_chat_api_key').val(apiKey);
                    $('[name=velaro_chat_account_linked').val('Yes');
                    $('#velaro-chat-form').submit();
                },
                error: function (result) {
                    $('[name=velaro_chat_site_identifier').val(null);
                    $('[name=velaro_chat_api_key').val(null);
                    $('[name=velaro_chat_account_linked').val('No');
                    $('#velaro-chat-form').submit();
                }
            });

        });
        matchVelaroGroup();
    });
} (jQuery));
