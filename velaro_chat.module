<?php 

/**
 * Plugin for adding Velaro live chat to your drupal site
 */


 /**
 * Page callback: Velaro settings
 *
 * @see velaro_chat_menu()
 */
 function velaro_chat_form($form, &$form_state) 
 {
   $form['velaro_chat_site_identifier'] = array(
     '#type' => 'hidden',
     '#default_value' => variable_get('velaro_chat_site_identifier'),
   );

   $form['velaro_chat_api_key'] = array(
     '#type' => 'hidden',
     '#default_value' => variable_get('velaro_chat_api_key'),
   );

   $form['velaro_chat_account_linked'] = array(
     '#type' => 'hidden',
     '#default_value' => variable_get('velaro_chat_account_linked'),
   );

   $form['velaro_chat_page_assignments'] = array(
     '#type' => 'hidden',
     '#default_value' => variable_get('velaro_chat_page_assignments'),
   );

   if (variable_get('velaro_chat_account_linked') == 'No') {
     $form['velaro_account_linking_error'] = array(
       '#type' => 'item',
       '#markup' => '<div style="color:red;">** Error linking account, please check your username and password **</div>'
     );
   }
   
   $form['velaro_chat_styles'] = array(
     '#type' => 'item',
     '#markup' => '<style>
     .velaro-form-header {
               border-bottom: 1px solid rgba(0, 0, 0, 0.05);
               font-weight: bold;
               color: #23282d;
               font-size: 1.3em;
               vertical-align: middle;
               padding: 10px 0 0 10px;
               margin: 0 5px -10px 5px;
           }
           .form-submit{
               margin-left:350px;
               }</style>'
   );

   $form['velaro_chat_link_account_header'] = array(
     '#type' => 'item',
     '#markup' => '<div class="velaro-form-header">Link Account</div>'
   );

   $form['velaro_chat_username'] = array(
     '#type' => 'textfield',
     '#title' => t('Velaro username'),
     '#default_value' => variable_get('velaro_chat_username'),
     '#description' => t('The email address for the velaro account.'),
     '#required' => true,
   );
 
   $form['velaro_chat_password'] = array(
     '#type' => 'password',
     '#title' => t('Velaro password'),
     '#default_value' => variable_get('velaro_chat_password'),
   );

   $form['velaro_chat_account_linked_label'] = array(
     '#type' => 'item',
     '#title' => t('Is account linked?'),
     '#markup' => t(variable_get('velaro_chat_account_linked', 'No'))
   );

   $form['velaro_chat_link_button'] = array(
     '#prefix' => '<input type="button" class="form-submit" value="' . t('Link Account') . '"id="velaro-link-account" />',
     '#suffix' => ''
   );

   $form['velaro_chat_plugin_settings_header'] = array(
     '#type' => 'item',
     '#markup' => '<div class="velaro-form-header">Settings</div>'
   );

   $form['velaro_chat_vm_enabled'] = array(
     '#type' => 'checkbox',
     '#title' => t('Enable visitor monitoring'),
     '#default_value' => variable_get('velaro_chat_vm_enabled', 0),
   );

   if (variable_get('velaro_chat_account_linked') == 'Yes') {
     $form['velaro_chat_routing_header'] = array(
       '#type' => 'item',
       '#markup' => '<div class="velaro-form-header">Chat Routing</div>'
     );

     $result = db_select('node', 'n')
     ->fields('n')
     ->execute();

     $results = array();

     while ($resultItem = $result->fetchAssoc())
     {
         $results[$resultItem['nid']] = $resultItem['title'];
     }

     $form['velaro_chat_pages'] = array(
       '#type' => 'select',
       '#title' => t('When on page'),
       '#options' => $results,
     );

     $curl = curl_init('https://app.velaro.com/api/plugins/groups');
     curl_setopt($curl, CURLOPT_HTTPHEADER, 
       array(
         "APIKEY:" . variable_get('velaro_chat_api_key'),
         "Content-Type: application/json",
     ));
     curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
     $res = curl_exec($curl);
     $groupResult = json_decode($res, true);
     $groups = array();
     $groups[0] = 'Default Group';

     foreach ($groupResult as $resultItem)
     {
       $groups[$resultItem['Key']] = $resultItem['Value'];
     }

     $form['velaro_chat_groups'] = array(
       '#type' => 'select',
       '#title' => t('Route to group'),
       '#options' => $groups,
     );

     $form['#attached']['js'] = array(
       'type' => 'js',
       'file' => drupal_get_path('module', 'velaro_chat') . '/velaro.config.js'
     );
   }
   return system_settings_form($form);
 }


/**
 * Implements hook_menu().
 *
 * Displays configuration form.
 *
 */
 function velaro_chat_menu() 
 {
   $items = array();
   $items['admin/config/content/velaro_chat'] = array(
     'title' => 'Velaro Live Chat',
     'description' => 'Configuration for Velaro live chat',
     'page callback' => 'drupal_get_form',
     'page arguments' => array('velaro_chat_form'),
     'access arguments' => array('administer site configuration'),
     'type' => MENU_NORMAL_ITEM,
   );
 
   return $items;
 }
 
 /**
  * Implements hook_uninstall()
  *
  * Removes variables installed by plugin
  *
  */
  function velaro_chat_uninstall(){
    $query = db_select('node','n');
    $query->fields('n', array('nid'))
            ->condition('type', 'velaro_chat');
    $data=$query->execute();
    while($result=$data->fetchObject()){
        node_delete($result->nid);

    }
    drupal_flush_all_caches();
  }
 
 /**
  * Implements hook_page_alter().
  *
  * Load chat scripts into page
  *
  */
 function velaro_chat_page_alter(&$page) {
   if (!path_is_admin(current_path()) && variable_get('velaro_chat_account_linked', 'No') == 'Yes') {
     if (arg(0) == 'node' && is_numeric(arg(1))) {
           $nid = arg(1);
     }
     if (!isset($nid) && drupal_is_front_page()) {
           $arg = drupal_get_normal_path(variable_get('site_frontpage', 'node'));
               if ($arg[0] == 'node' && is_numeric($arg[1])) {
                   $nid = $arg[1];
               }
     }
     $velaro_chat_settings = array(
       'page_assignments' => variable_get('velaro_chat_page_assignments'),
       'page_id' => isset($nid) ? $nid : 0,
       'site_id' => variable_get('velaro_chat_site_identifier'),
       'mobile_enabled' => variable_get('velaro_chat_mobile_enabled'),
       'vm_enabled' => variable_get('velaro_chat_vm_enabled'),
     );
     
     $options = array(
       'scope' => 'footer',
       'type' => 'external',
     );
     
     drupal_add_js("https://galleryuseastprod.blob.core.windows.net/velaroscripts/" . variable_get('velaro_chat_site_identifier') ."/globals.js", $options);
     drupal_add_js('https://galleryuseastprod.blob.core.windows.net/velaroscripts/velaro.loadscripts.js', $options);
     $options['type']= 'setting';
     drupal_add_js(array('velaro_chat' => $velaro_chat_settings), $options);
     $options['type'] = 'file';
     drupal_add_js(drupal_get_path('module', 'velaro_chat') . '/velaro.chatscripts.js', $options);
   }
 }

