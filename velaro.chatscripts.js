/**
 * Set values needed for chat
 */
var pageAssignments = Drupal.settings.velaro_chat.page_assignments ? JSON.parse(Drupal.settings.velaro_chat.page_assignments) : {};
var group = pageAssignments['page_' + Drupal.settings.velaro_chat.page_id] || 0;
Velaro.Globals.ActiveSite = Drupal.settings.velaro_chat.site_id;
Velaro.Globals.ActiveGroup = group;
Velaro.Globals.InlineEnabled = true;
Velaro.Globals.VisitorMonitoringEnabled = Drupal.settings.velaro_chat.vm_enabled == 1;
Velaro.Globals.InlinePosition = 0;
